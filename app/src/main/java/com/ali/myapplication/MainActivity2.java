package com.ali.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.ali.myapplication.databinding.ActivityMain2Binding;

import java.util.ArrayList;

public class MainActivity2 extends AppCompatActivity {
    private ActivityMain2Binding binding;
    String s = "";
    String i = "";
    float h = 0;
    int bilish = 0, j2 = -1;
    String n = "";
    String n1 = "";
    String n2 = "";
    String natija = "";
    int oz = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMain2Binding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());


//        Ishoralar


        binding.buton17.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (s.isEmpty()){

                }else {
                    if (s.substring(s.length() - 1).equals("^")||s.substring(s.length() - 1).equals("+") || s.substring(s.length() - 1).equals("-") || s.substring(s.length() - 1).equals("x") || s.substring(s.length() - 1).equals("/") || s.substring(s.length() - 1).equals("%")) {
                        s = s.substring(0, s.length() - 1);
                        i += "^";
                        n = s;
                        s += i;
                        i = "";
                        binding.text.setText(s);
                        binding.text1.setText(s);
                        binding.text.setVisibility(View.VISIBLE);
                        binding.text1.setVisibility(View.VISIBLE);
                    } else {
                        i += "^";
                        n = s;
                        s += i;
                        i = "";
                        bilish = 0;
                        int j1 = 0;

                        for (int j = 1; j < s.length(); j++) {
                            if (s.startsWith("^", j)||s.startsWith("x", j) || s.startsWith("%", j) || s.startsWith("/", j) || s.startsWith("+", j) || s.startsWith("-", j)) {
                                j1++;
                                if (j1 == 2) {
                                    j2 = j;

                                }
                            }
                        }

                        binding.text.setText(s);
                        binding.text1.setText(s);
                        binding.text.setVisibility(View.VISIBLE);
                        binding.text1.setVisibility(View.VISIBLE);

                    }
                }
            }
        });

        binding.buton3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (s.isEmpty()) {

                } else {

                    if (s.substring(s.length() - 1).equals("^")||s.substring(s.length() - 1).equals("+") || s.substring(s.length() - 1).equals("-") || s.substring(s.length() - 1).equals("x") || s.substring(s.length() - 1).equals("/") || s.substring(s.length() - 1).equals("%")) {
                        s = s.substring(0, s.length() - 1);
                        i += "%";
                        n = s;
                        s += i;
                        i = "";
                        binding.text.setText(s);
                        binding.text1.setText(s);
                        binding.text.setVisibility(View.VISIBLE);
                        binding.text1.setVisibility(View.VISIBLE);
                    } else {
                        i += "%";
                        n = s;
                        s += i;
                        i = "";
                        bilish = 0;
                        int j1 = 0;

                        for (int j = 1; j < s.length(); j++) {
                            if (s.startsWith("^", j)||s.startsWith("x", j) || s.startsWith("%", j) || s.startsWith("/", j) || s.startsWith("+", j) || s.startsWith("-", j)) {
                                j1++;
                                if (j1 == 2) {
                                    j2 = j;

                                }
                            }
                        }

                        binding.text.setText(s);
                        binding.text1.setText(s);
                        binding.text.setVisibility(View.VISIBLE);
                        binding.text1.setVisibility(View.VISIBLE);

                    }


                }
            }
        });
        binding.buton4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (s.isEmpty()) {

                } else {
                    if (s.substring(s.length() - 1).equals("^")||s.substring(s.length() - 1).equals("+") || s.substring(s.length() - 1).equals("-") || s.substring(s.length() - 1).equals("x") || s.substring(s.length() - 1).equals("/") || s.substring(s.length() - 1).equals("%")) {
                        s = s.substring(0, s.length() - 1);
                        i += "/";
                        n = s;
                        s += i;
                        i = "";
                        binding.text.setText(s);
                        binding.text1.setText(s);
                        binding.text.setVisibility(View.VISIBLE);
                        binding.text1.setVisibility(View.VISIBLE);
                    } else {
                        i += "/";
                        n = s;
                        s += i;
                        i = "";
                        bilish = 0;
                        int j1 = 0;
                        boolean b = false;
                        for (int j = 1; j < s.length(); j++) {
                            if (s.startsWith("^", j)||s.startsWith("x", j) || s.startsWith("%", j) || s.startsWith("/", j) || s.startsWith("+", j) || s.startsWith("-", j)) {
                                j1++;
                                if (j1 == 2) {
                                    j2 = j;
                                    b = true;
                                }
                            }
                        }

                        binding.text.setText(s);
                        binding.text1.setText(s);
                        binding.text.setVisibility(View.VISIBLE);
                        binding.text1.setVisibility(View.VISIBLE);

                    }
                }
            }
        });
        binding.buton8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (s.isEmpty()) {

                } else {
                    if (s.substring(s.length() - 1).equals("^")||s.substring(s.length() - 1).equals("+") || s.substring(s.length() - 1).equals("-") || s.substring(s.length() - 1).equals("x") || s.substring(s.length() - 1).equals("/") || s.substring(s.length() - 1).equals("%")) {
                        s = s.substring(0, s.length() - 1);
                        i += "x";
                        n = s;
                        s += i;
                        i = "";
                        binding.text.setText(s);
                        binding.text1.setText(s);
                        binding.text.setVisibility(View.VISIBLE);
                        binding.text1.setVisibility(View.VISIBLE);
                    } else {
                        i += "x";
                        n = s;
                        s += i;
                        i = "";
                        bilish = 0;
                        int j1 = 0;

                        for (int j = 1; j < s.length(); j++) {
                            if (s.startsWith("^", j)||s.startsWith("x", j) || s.startsWith("%", j) || s.startsWith("/", j) || s.startsWith("+", j) || s.startsWith("-", j)) {
                                j1++;
                                if (j1 == 2) {
                                    j2 = j;

                                }
                            }
                        }

                        binding.text.setText(s);
                        binding.text1.setText(s);
                        binding.text.setVisibility(View.VISIBLE);
                        binding.text1.setVisibility(View.VISIBLE);
                    }
                }
            }
        });
        binding.buton12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (s.isEmpty()) {
                    s += "-";
                    binding.text1.setText(s);

                } else {
                    if (s.substring(s.length() - 1).equals("^")||s.substring(s.length() - 1).equals("+") || s.substring(s.length() - 1).equals("-") || s.substring(s.length() - 1).equals("x") || s.substring(s.length() - 1).equals("/") || s.substring(s.length() - 1).equals("%")) {
                        s = s.substring(0, s.length() - 1);
                        i += "-";
                        n = s;
                        s += i;
                        i = "";

                        binding.text.setText(s);
                        binding.text1.setText(s);
                        binding.text.setVisibility(View.VISIBLE);
                        binding.text1.setVisibility(View.VISIBLE);
                    } else {
                        i += "-";
                        n = s;
                        s += i;
                        i = "";
                        bilish = 0;
                        int j1 = 0;

                        for (int j = 1; j < s.length(); j++) {
                            if (s.startsWith("^", j)||s.startsWith("x", j) || s.startsWith("%", j) || s.startsWith("/", j) || s.startsWith("+", j) || s.startsWith("-", j)) {
                                j1++;
                                if (j1 == 2) {
                                    j2 = j;

                                }
                            }
                        }

                        binding.text.setText(s);
                        binding.text1.setText(s);
                        binding.text.setVisibility(View.VISIBLE);
                        binding.text1.setVisibility(View.VISIBLE);
                    }
                }
            }
        });
        binding.buton16.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (s.isEmpty()) {


                } else {
                    if (s.substring(s.length() - 1).equals("^")||s.substring(s.length() - 1).equals("+") || s.substring(s.length() - 1).equals("-") || s.substring(s.length() - 1).equals("x") || s.substring(s.length() - 1).equals("/") || s.substring(s.length() - 1).equals("%")) {
                        s = s.substring(0, s.length() - 1);
                        i += "+";
                        n = s;
                        s += i;
                        i = "";
                        binding.text.setText(s);
                        binding.text1.setText(s);
                        binding.text.setVisibility(View.VISIBLE);
                        binding.text1.setVisibility(View.VISIBLE);
                    } else {
                        i += "+";
                        n = s;
                        s += i;
                        i = "";
                        bilish = 0;
                        int j1 = 0;

                        for (int j = 1; j < s.length(); j++) {
                            if (s.startsWith("^", j)||s.startsWith("x", j) || s.startsWith("%", j) || s.startsWith("/", j) || s.startsWith("+", j) || s.startsWith("-", j)) {
                                j1++;
                                if (j1 == 2) {
                                    j2 = j;

                                }
                            }
                        }

                        binding.text.setText(s);
                        binding.text1.setText(s);
                        binding.text.setVisibility(View.VISIBLE);
                        binding.text1.setVisibility(View.VISIBLE);
                    }
                }
            }
        });


//        Sonlar


        binding.buton19.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bilish == 1) {
                    binding.text2.setText(natija + "=" + s);
                    bilish = 0;
                    natija = "";
                    s = "";
                }
                if (s.isEmpty() || s.equals("0.")) {

                    s = "0.";
                    binding.text.setText(s);
                    binding.text1.setText(s);
                    binding.text.setVisibility(View.VISIBLE);
                    binding.text1.setVisibility(View.VISIBLE);
                } else {
                    s += ".";
                    binding.text.setText(s);
                    binding.text1.setText(s);
                    binding.text.setVisibility(View.VISIBLE);
                    binding.text1.setVisibility(View.VISIBLE);
                }
            }
        });
        binding.buton18.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bilish == 1) {
                    binding.text2.setText(natija + "=" + s);
                    bilish = 0;
                    natija = "";
                    s = "";
                }
                if (s.isEmpty()) {
                    s = "0";
                    binding.text.setText(s);
                    binding.text1.setText(s);
                    binding.text.setVisibility(View.VISIBLE);
                    binding.text1.setVisibility(View.VISIBLE);
                } else {
                    s += "0";
                    binding.text.setText(s);
                    binding.text1.setText(s);
                    binding.text.setVisibility(View.VISIBLE);
                    binding.text1.setVisibility(View.VISIBLE);
                }

            }
        });
        binding.buton13.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bilish == 1) {
                    binding.text2.setText(natija + "=" + s);
                    bilish = 0;
                    natija = "";
                    s = "";
                }
                s += "1";
                binding.text.setText(s);
                binding.text1.setText(s);
                binding.text.setVisibility(View.VISIBLE);
                binding.text1.setVisibility(View.VISIBLE);

            }
        });
        binding.buton14.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bilish == 1) {
                    binding.text2.setText(natija + "=" + s);
                    bilish = 0;
                    natija = "";
                    s = "";
                }
                s += "2";
                binding.text.setText(s);
                binding.text1.setText(s);
                binding.text.setVisibility(View.VISIBLE);
                binding.text1.setVisibility(View.VISIBLE);

            }
        });
        binding.buton15.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bilish == 1) {
                    binding.text2.setText(natija + "=" + s);
                    bilish = 0;
                    natija = "";
                    s = "";
                }
                s += "3";
                binding.text.setText(s);
                binding.text1.setText(s);
                binding.text.setVisibility(View.VISIBLE);
                binding.text1.setVisibility(View.VISIBLE);

            }
        });
        binding.buton9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bilish == 1) {
                    binding.text2.setText(natija + "=" + s);
                    bilish = 0;
                    natija = "";
                    s = "";
                }
                s += "4";
                binding.text.setText(s);
                binding.text1.setText(s);
                binding.text.setVisibility(View.VISIBLE);
                binding.text1.setVisibility(View.VISIBLE);

            }
        });
        binding.buton10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bilish == 1) {
                    binding.text2.setText(natija + "=" + s);
                    bilish = 0;
                    natija = "";
                    s = "";
                }
                s += "5";
                binding.text.setText(s);
                binding.text1.setText(s);
                binding.text.setVisibility(View.VISIBLE);
                binding.text1.setVisibility(View.VISIBLE);

            }
        });
        binding.buton11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bilish == 1) {
                    binding.text2.setText(natija + "=" + s);
                    bilish = 0;
                    natija = "";
                    s = "";
                }
                s += "6";
                binding.text.setText(s);
                binding.text1.setText(s);
                binding.text.setVisibility(View.VISIBLE);
                binding.text1.setVisibility(View.VISIBLE);

            }
        });
        binding.buton5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bilish == 1) {
                    binding.text2.setText(natija + "=" + s);
                    bilish = 0;
                    natija = "";
                    s = "";
                }
                s += "7";
                binding.text.setText(s);
                binding.text1.setText(s);
                binding.text.setVisibility(View.VISIBLE);
                binding.text1.setVisibility(View.VISIBLE);

            }
        });
        binding.buton6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bilish == 1) {
                    binding.text2.setText(natija + "=" + s);
                    bilish = 0;
                    natija = "";
                    s = "";
                }
                s += "8";
                binding.text.setText(s);
                binding.text1.setText(s);
                binding.text.setVisibility(View.VISIBLE);
                binding.text1.setVisibility(View.VISIBLE);

            }
        });
        binding.buton7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (bilish == 1) {
                    binding.text2.setText(natija + "=" + s);
                    bilish = 0;
                    natija = "";
                    s = "";
                }
                s += "9";
                binding.text.setText(s);
                binding.text1.setText(s);
                binding.text.setVisibility(View.VISIBLE);
                binding.text1.setVisibility(View.VISIBLE);
            }
        });


//        NAtija


        binding.buton20.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (s.isEmpty()) {

                } else {
                    if (j2 == -1) { if (s.substring(s.length()-1).equals("^")||s.substring(s.length()-1).equals("%")||s.substring(s.length()-1).equals("x")||s.substring(s.length()-1).equals("/")||s.substring(s.length()-1).equals("+")||s.substring(s.length()-1).equals("-")){
                        Toast.makeText(MainActivity2.this, "Belgidan keyin son kelishi kerak", Toast.LENGTH_SHORT).show();
                    }else {
                        if (s.substring(n.length(), n.length() + 1).equals("x")) {
                            n1 = s.substring(n.length() + 1);
                            h = Float.parseFloat(n) * Float.parseFloat(n1);
                            natija = s;
                            binding.text.setText(s);
                            s = String.valueOf(h);
                            binding.text1.setText("=" + s);
                            bilish = 1;
                        } else if (s.substring(n.length(), n.length() + 1).equals("/")) {
                            n1 = s.substring(n.length() + 1);
                            h = Float.parseFloat(n) / Float.parseFloat(n1);
                            natija = s;
                            binding.text.setText(s);
                            s = String.valueOf(h);
                            binding.text1.setText("=" + s);
                            bilish = 1;
                        } else if (s.substring(n.length(), n.length() + 1).equals("+")) {
                            n1 = s.substring(n.length() + 1);
                            h = Float.parseFloat(n) + Float.parseFloat(n1);
                            natija = s;
                            binding.text.setText(s);
                            s = String.valueOf(h);
                            binding.text1.setText("=" + s);
                            bilish = 1;
                        } else if (s.substring(n.length(), n.length() + 1).equals("-")) {
                            n1 = s.substring(n.length() + 1);
                            h = Float.parseFloat(n) - Float.parseFloat(n1);
                            natija = s;
                            binding.text.setText(s);
                            s = String.valueOf(h);
                            binding.text1.setText("=" + s);
                            bilish = 1;
                        } else if (s.substring(n.length(), n.length() + 1).equals("%")) {
                            n1 = s.substring(n.length() + 1);
                            h = Float.parseFloat(n) * Float.parseFloat(n1) / 100;
                            natija = s;
                            binding.text.setText(s);
                            s = String.valueOf(h);
                            binding.text1.setText("=" + s);
                            bilish = 1;
                        }else if (s.substring(n.length(), n.length() + 1).equals("^")) {
                            n1 = s.substring(n.length() + 1);
                            h = (float) Math.pow(Float.parseFloat(n),Float.parseFloat(n1));
                            natija = s;
                            binding.text.setText(s);
                            s = String.valueOf(h);
                            binding.text1.setText("=" + s);
                            bilish = 1;
                        }}
                    } else {
                        if (s.substring(s.length()-1).equals("^")||s.substring(s.length()-1).equals("%")||s.substring(s.length()-1).equals("x")||s.substring(s.length()-1).equals("/")||s.substring(s.length()-1).equals("+")||s.substring(s.length()-1).equals("-")){
                            Toast.makeText(MainActivity2.this, "Belgidan keyin son kelishi kerak", Toast.LENGTH_SHORT).show();
                        }else {
                            int j1 = 0, j3 = 0;
                            for (int j = 0; j < s.length(); j++) {
                                if (s.startsWith("x", j) || s.startsWith("%", j) || s.startsWith("/", j) || s.startsWith("+", j) || s.startsWith("-", j)) {
                                    j3 = j;
                                    break;
                                }
                            }
                            n = s.substring(0, j3);
                            n1 = s.substring(j3 + 1, j2);
                            n2 = s.substring(j2 + 1);
                            if (s.substring(j3, j3 + 1).equals("%") && s.substring(j2, j2 + 1).equals("x")) {
                                h = Float.parseFloat(n) * Float.parseFloat(n1) / 100 * Float.parseFloat(n2);
                                natija = s;
                                binding.text.setText(s);
                                s = String.valueOf(h);
                                binding.text1.setText("=" + s);
                                bilish = 1;
                            }
                            if (s.substring(j3, j3 + 1).equals("%") && s.substring(j2, j2 + 1).equals("/")) {
                                h = (Float.parseFloat(n) * Float.parseFloat(n1) / 100) / Float.parseFloat(n2);
                                natija = s;
                                binding.text.setText(s);
                                s = String.valueOf(h);
                                binding.text1.setText("=" + s);
                                bilish = 1;
                            }
                            if (s.substring(j3, j3 + 1).equals("%") && s.substring(j2, j2 + 1).equals("%")) {
                                h = Float.parseFloat(n) * Float.parseFloat(n1) / 100 * Float.parseFloat(n2) / 100;
                                natija = s;
                                binding.text.setText(s);
                                s = String.valueOf(h);
                                binding.text1.setText("=" + s);
                                bilish = 1;
                            }
                            if (s.substring(j3, j3 + 1).equals("%") && s.substring(j2, j2 + 1).equals("^")) {
                                h = (float) Math.pow(Float.parseFloat(n) * Float.parseFloat(n1) / 100, Float.parseFloat(n2));
                                natija = s;
                                binding.text.setText(s);
                                s = String.valueOf(h);
                                binding.text1.setText("=" + s);
                                bilish = 1;
                            }
                            if (s.substring(j3, j3 + 1).equals("%") && s.substring(j2, j2 + 1).equals("+")) {
                                h = Float.parseFloat(n) * Float.parseFloat(n1) / 100 + Float.parseFloat(n2) / 100;
                                natija = s;
                                binding.text.setText(s);
                                s = String.valueOf(h);
                                binding.text1.setText("=" + s);
                                bilish = 1;
                            }
                            if (s.substring(j3, j3 + 1).equals("%") && s.substring(j2, j2 + 1).equals("-")) {
                                h = Float.parseFloat(n) * Float.parseFloat(n1) / 100 - Float.parseFloat(n2) / 100;
                                natija = s;
                                binding.text.setText(s);
                                s = String.valueOf(h);
                                binding.text1.setText("=" + s);
                                bilish = 1;
                            }
                            if (s.substring(j3, j3 + 1).equals("/") && s.substring(j2, j2 + 1).equals("%")) {
                                h = Float.parseFloat(n) / (Float.parseFloat(n1) * Float.parseFloat(n2) / 100);
                                natija = s;
                                binding.text.setText(s);
                                s = String.valueOf(h);
                                binding.text1.setText("=" + s);
                                bilish = 1;
                            }

                            if (s.substring(j3, j3 + 1).equals("/") && s.substring(j2, j2 + 1).equals("/")) {
                                h = (Float.parseFloat(n) / Float.parseFloat(n1)) / Float.parseFloat(n2);
                                natija = s;
                                binding.text.setText(s);
                                s = String.valueOf(h);
                                binding.text1.setText("=" + s);
                                bilish = 1;
                            }
                            if (s.substring(j3, j3 + 1).equals("/") && s.substring(j2, j2 + 1).equals("x")) {
                                h = (Float.parseFloat(n) / Float.parseFloat(n1)) * Float.parseFloat(n2);
                                natija = s;
                                binding.text.setText(s);
                                s = String.valueOf(h);
                                binding.text1.setText("=" + s);
                                bilish = 1;

                            }
                            if (s.substring(j3, j3 + 1).equals("/") && s.substring(j2, j2 + 1).equals("^")) {
                                h = (float) (Float.parseFloat(n) / Math.pow(Float.parseFloat(n1), Float.parseFloat(n2)));
                                natija = s;
                                binding.text.setText(s);
                                s = String.valueOf(h);
                                binding.text1.setText("=" + s);
                                bilish = 1;

                            }
                            if (s.substring(j3, j3 + 1).equals("/") && s.substring(j2, j2 + 1).equals("+")) {
                                h = Float.parseFloat(n) / Float.parseFloat(n1) + Float.parseFloat(n2);
                                natija = s;
                                binding.text.setText(s);
                                s = String.valueOf(h);
                                binding.text1.setText("=" + s);
                                bilish = 1;
                            }
                            if (s.substring(j3, j3 + 1).equals("/") && s.substring(j2, j2 + 1).equals("-")) {
                                h = Float.parseFloat(n) / Float.parseFloat(n1) - Float.parseFloat(n2);
                                natija = s;
                                binding.text.setText(s);
                                s = String.valueOf(h);
                                binding.text1.setText("=" + s);
                                bilish = 1;
                            }
                            if (s.substring(j3, j3 + 1).equals("x") && s.substring(j2, j2 + 1).equals("%")) {
                                h = Float.parseFloat(n) * (Float.parseFloat(n1) * Float.parseFloat(n2) / 100);
                                natija = s;
                                binding.text.setText(s);
                                s = String.valueOf(h);
                                binding.text1.setText("=" + s);
                                bilish = 1;
                            }

                            if (s.substring(j3, j3 + 1).equals("x") && s.substring(j2, j2 + 1).equals("/")) {
                                h = (Float.parseFloat(n) * (Float.parseFloat(n1)) / Float.parseFloat(n2));
                                natija = s;
                                binding.text.setText(s);
                                s = String.valueOf(h);
                                binding.text1.setText("=" + s);
                                bilish = 1;
                            }
                            if (s.substring(j3, j3 + 1).equals("x") && s.substring(j2, j2 + 1).equals("x")) {
                                h = (Float.parseFloat(n) * Float.parseFloat(n1)) * Float.parseFloat(n2);
                                natija = s;
                                binding.text.setText(s);
                                s = String.valueOf(h);
                                binding.text1.setText("=" + s);
                                bilish = 1;

                            }
                            if (s.substring(j3, j3 + 1).equals("x") && s.substring(j2, j2 + 1).equals("^")) {
                                h = (float) (Float.parseFloat(n) * Math.pow(Float.parseFloat(n1), Float.parseFloat(n2)));
                                natija = s;
                                binding.text.setText(s);
                                s = String.valueOf(h);
                                binding.text1.setText("=" + s);
                                bilish = 1;

                            }
                            if (s.substring(j3, j3 + 1).equals("x") && s.substring(j2, j2 + 1).equals("+")) {
                                h = Float.parseFloat(n) * Float.parseFloat(n1) + Float.parseFloat(n2);
                                natija = s;
                                binding.text.setText(s);
                                s = String.valueOf(h);
                                binding.text1.setText("=" + s);
                                bilish = 1;
                            }
                            if (s.substring(j3, j3 + 1).equals("x") && s.substring(j2, j2 + 1).equals("-")) {
                                h = Float.parseFloat(n) * Float.parseFloat(n1) - Float.parseFloat(n2);
                                natija = s;
                                binding.text.setText(s);
                                s = String.valueOf(h);
                                binding.text1.setText("=" + s);
                                bilish = 1;
                            }
                            if (s.substring(j3, j3 + 1).equals("+") && s.substring(j2, j2 + 1).equals("%")) {
                                h = Float.parseFloat(n) + (Float.parseFloat(n1) * Float.parseFloat(n2) / 100);
                                natija = s;
                                binding.text.setText(s);
                                s = String.valueOf(h);
                                binding.text1.setText("=" + s);
                                bilish = 1;
                            }

                            if (s.substring(j3, j3 + 1).equals("+") && s.substring(j2, j2 + 1).equals("/")) {
                                h = (Float.parseFloat(n) + (Float.parseFloat(n1)) / Float.parseFloat(n2));
                                natija = s;
                                binding.text.setText(s);
                                s = String.valueOf(h);
                                binding.text1.setText("=" + s);
                                bilish = 1;
                            }
                            if (s.substring(j3, j3 + 1).equals("+") && s.substring(j2, j2 + 1).equals("x")) {
                                h = Float.parseFloat(n) + Float.parseFloat(n1) * Float.parseFloat(n2);
                                natija = s;
                                binding.text.setText(s);
                                s = String.valueOf(h);
                                binding.text1.setText("=" + s);
                                bilish = 1;

                            }
                            if (s.substring(j3, j3 + 1).equals("+") && s.substring(j2, j2 + 1).equals("^")) {
                                h = (float) (Float.parseFloat(n) + Math.pow(Float.parseFloat(n1), Float.parseFloat(n2)));
                                natija = s;
                                binding.text.setText(s);
                                s = String.valueOf(h);
                                binding.text1.setText("=" + s);
                                bilish = 1;

                            }
                            if (s.substring(j3, j3 + 1).equals("+") && s.substring(j2, j2 + 1).equals("+")) {
                                h = Float.parseFloat(n) + Float.parseFloat(n1) + Float.parseFloat(n2);
                                natija = s;
                                binding.text.setText(s);
                                s = String.valueOf(h);
                                binding.text1.setText("=" + s);
                                bilish = 1;
                            }
                            if (s.substring(j3, j3 + 1).equals("+") && s.substring(j2, j2 + 1).equals("-")) {
                                h = Float.parseFloat(n) + Float.parseFloat(n1) - Float.parseFloat(n2);
                                natija = s;
                                binding.text.setText(s);
                                s = String.valueOf(h);
                                binding.text1.setText("=" + s);
                                bilish = 1;
                            }
                            if (s.substring(j3, j3 + 1).equals("-") && s.substring(j2, j2 + 1).equals("%")) {
                                h = Float.parseFloat(n) - (Float.parseFloat(n1) * Float.parseFloat(n2) / 100);
                                natija = s;
                                binding.text.setText(s);
                                s = String.valueOf(h);
                                binding.text1.setText("=" + s);
                                bilish = 1;
                            }

                            if (s.substring(j3, j3 + 1).equals("-") && s.substring(j2, j2 + 1).equals("/")) {
                                h = (Float.parseFloat(n) - (Float.parseFloat(n1)) / Float.parseFloat(n2));
                                natija = s;
                                binding.text.setText(s);
                                s = String.valueOf(h);
                                binding.text1.setText("=" + s);
                                bilish = 1;
                            }
                            if (s.substring(j3, j3 + 1).equals("-") && s.substring(j2, j2 + 1).equals("x")) {
                                h = Float.parseFloat(n) - Float.parseFloat(n1) * Float.parseFloat(n2);
                                natija = s;
                                binding.text.setText(s);
                                s = String.valueOf(h);
                                binding.text1.setText("=" + s);
                                bilish = 1;

                            }
                            if (s.substring(j3, j3 + 1).equals("-") && s.substring(j2, j2 + 1).equals("^")) {
                                h = (float) (Float.parseFloat(n) - Math.pow(Float.parseFloat(n1), Float.parseFloat(n2)));
                                natija = s;
                                binding.text.setText(s);
                                s = String.valueOf(h);
                                binding.text1.setText("=" + s);
                                bilish = 1;

                            }
                            if (s.substring(j3, j3 + 1).equals("-") && s.substring(j2, j2 + 1).equals("+")) {
                                h = Float.parseFloat(n) - Float.parseFloat(n1) + Float.parseFloat(n2);
                                natija = s;
                                binding.text.setText(s);
                                s = String.valueOf(h);
                                binding.text1.setText("=" + s);
                                bilish = 1;
                            }
                            if (s.substring(j3, j3 + 1).equals("-") && s.substring(j2, j2 + 1).equals("-")) {
                                h = Float.parseFloat(n) - Float.parseFloat(n1) - Float.parseFloat(n2);
                                natija = s;
                                binding.text.setText(s);
                                s = String.valueOf(h);
                                binding.text1.setText("=" + s);
                                bilish = 1;
                            }
                            if (s.substring(j3, j3 + 1).equals("^") && s.substring(j2, j2 + 1).equals("%")) {
                                h = (float) Math.pow(Float.parseFloat(n), (Float.parseFloat(n1) * Float.parseFloat(n2) / 100));
                                natija = s;
                                binding.text.setText(s);
                                s = String.valueOf(h);
                                binding.text1.setText("=" + s);
                                bilish = 1;
                            }

                            if (s.substring(j3, j3 + 1).equals("^") && s.substring(j2, j2 + 1).equals("/")) {
                                h = (float) (((Math.pow(Float.parseFloat(n), Float.parseFloat(n1)))) / Float.parseFloat(n2));
                                natija = s;
                                binding.text.setText(s);
                                s = String.valueOf(h);
                                binding.text1.setText("=" + s);
                                bilish = 1;
                            }
                            if (s.substring(j3, j3 + 1).equals("^") && s.substring(j2, j2 + 1).equals("x")) {
                                h = (float) (Math.pow(Float.parseFloat(n), Float.parseFloat(n1)) * Float.parseFloat(n2));
                                natija = s;
                                binding.text.setText(s);
                                s = String.valueOf(h);
                                binding.text1.setText("=" + s);
                                bilish = 1;

                            }
                            if (s.substring(j3, j3 + 1).equals("^") && s.substring(j2, j2 + 1).equals("^")) {
                                h = (float) (Math.pow(Float.parseFloat(n), Math.pow(Float.parseFloat(n1), Float.parseFloat(n2))));
                                natija = s;
                                binding.text.setText(s);
                                s = String.valueOf(h);
                                binding.text1.setText("=" + s);
                                bilish = 1;

                            }
                            if (s.substring(j3, j3 + 1).equals("^") && s.substring(j2, j2 + 1).equals("+")) {
                                h = (float) (Math.pow(Float.parseFloat(n), Float.parseFloat(n1)) + Float.parseFloat(n2));
                                natija = s;
                                binding.text.setText(s);
                                s = String.valueOf(h);
                                binding.text1.setText("=" + s);
                                bilish = 1;
                            }
                            if (s.substring(j3, j3 + 1).equals("^") && s.substring(j2, j2 + 1).equals("-")) {
                                h = (float) (Math.pow(Float.parseFloat(n), Float.parseFloat(n1)) - Float.parseFloat(n2));
                                natija = s;
                                binding.text.setText(s);
                                s = String.valueOf(h);
                                binding.text1.setText("=" + s);
                                bilish = 1;
                            }
                            j2 = -1;
                        }
                    }

                }
            }

        });


//        O'chirgichlar


        binding.buton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                oz++;
                s = "";
                i = "";
                h = 0;
                bilish = 0;
                j2 = -1;
                n = "";
                n1 = "";
                n2 = "";
                natija = "";
                binding.text.setText("0");
                binding.text1.setText("0");
                if (oz == 2) {
                    binding.text2.setText("");
                    oz = 0;
                }

            }
        });


        binding.buton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (s.isEmpty()){

                }else {
                s = s.substring(0, s.length() - 1);
                binding.text.setText(s);
                binding.text1.setText(s);}
            }
        });



    }
}